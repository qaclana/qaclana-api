/*
 * Copyright 2017 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.api;

/**
 * @author Juraci Paixão Kröhling
 */
public interface Configuration {
    String BIND                      = "qaclana.bind";
    String PORT                      = "qaclana.port";
    String HEALTHCHECK_BIND          = "qaclana.healthcheck.bind";
    String HEALTHCHECK_PORT          = "qaclana.healthcheck.port";
    String RPC_PORT                  = "qaclana.rpc.port";
    String TARGET                    = "qaclana.target";

    String BACKEND_HOSTNAME          = "qaclana.backend.hostname";
    String BACKEND_PORT              = "qaclana.backend.port";
    String BACKEND_RPC_PORT          = "qaclana.backend.rpc.port";
    String BACKEND_RPC_USE_PLAINTEXT = "qaclana.backend.rpc.plaintext";

    String SERVER_HOSTNAME           = "qaclana.server.hostname";
    String SERVER_PORT               = "qaclana.server.port";
    String SERVER_RPC_PORT           = "qaclana.server.rpc.port";
    String SERVER_RPC_USE_PLAINTEXT  = "qaclana.server.rpc.plaintext";

    String PROXY_HOSTNAME            = "qaclana.proxy.hostname";
    String PROXY_PORT                = "qaclana.proxy.port";
    String PROXY_RPC_PORT            = "qaclana.proxy.rpc.port";
    String PROXY_RPC_USE_PLAINTEXT   = "qaclana.proxy.rpc.plaintext";
}
